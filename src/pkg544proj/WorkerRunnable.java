package pkg544proj;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* WorkerRunnable class
* This class is the meat of the server class. It handles everything that the server
* will need to do based on whether it is a TCP or UDP server (which is specified
* when it is created). An important thing to note about this class is that the successive
* states begin at the bottom of the file, and increase as you go upwards.
*/
public class WorkerRunnable implements Runnable{

    protected Socket clientSocket = null;
    protected DatagramSocket clientDatagramSocket = null;
    protected String tlProtocol = "";
    protected String serverText   = null;
    protected DatagramPacket packet = null;
    protected static String serverfileLoc = "highlightsList"+ OcpConstants.getOSFileSep();
    protected int curMsgNum = 0;
    protected OcpFramer framer;
    protected OcpMsgCoder coder;
    private static InputStream in = null;
    private static OutputStream out = null;
    private static BufferedOutputStream bos = null;
    private static DataOutputStream dos = null;
    private static final int BUFSIZE = 32; //Size of receive buffer

    public WorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
        this.tlProtocol = "TCP";
    }
    
    public WorkerRunnable(DatagramSocket clientSocket, DatagramPacket p, String serverText) {
        this.clientDatagramSocket = clientSocket;
        this.packet = p;
        this.serverText   = serverText;
        this.tlProtocol = "UDP";
    }
    
    private void ConfirmClientReceive(){
    	//------------WAIT FOR CLIENT CONFIRM RECEIVE (STATE 5 AND STATE 6)---------------------------------------
    	/*
    	        WE'RE HERE BECAUSE WE HAVE FINISHED THE FILE TRANSFER TO THE CLIENT AND ARE
    	        WAITING FOR A POSITIVE RESPONSE FROM THE CLIENT TO CONFIRM THE FILES HAVE BEEN RECEIVED
    	        AND SUBSEQUENTLY CLOSE THE CONNECTION
    	*/
    	//------------------------------------------------------------------------------
    	//GET THE MESSAGE FROM THE FRAMER
        System.out.print("Receiving(TCP)...");
        OcpPDU mostRecentAnswer = null;
        byte[] rtnMsg;
        try {
            rtnMsg = framer.nextMsg();
            mostRecentAnswer = coder.fromWire(rtnMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //Message should have message number curMsgNum+1 and code 40
        if(mostRecentAnswer.getStatusCode() != OcpConstants.SC_TERMINATE || 
                            mostRecentAnswer.getMsgNumber() != (curMsgNum+1)){
            System.out.println("Invalid Status Code or Message Number");

            //Create the "Connection Killed" Response
            OcpPDU response = OcpConstants.makeTerminateConnectionNow();
            try {
                byte[] encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            curMsgNum = curMsgNum+2;//+1 from client +1 to send back
            System.out.println("Received!\n"
                    + "The following PDU was received from the client ");
            System.out.println(mostRecentAnswer.pduToString());

            OcpPDU response = mostRecentAnswer;
            response.setMsgNumber(curMsgNum);  
            response.setDateTime(System.currentTimeMillis());
            response.setStatusCode(OcpConstants.SC_TERMINATE);

            //Send the "Sending message" PDU
            byte[] encodedMsg;
            try {
                encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
   
    private void handleClientRequest(){
    	//------------GET CLIENT FILE REQUEST (STATE 5)---------------------------------------
    	/*
    	        HERE IS WHERE WE GET THE REQUEST FOR FILES FROM THE CLIENT AND SUBSEQUENTLY
    	        SEND THE REQUESTED FILES BACK TO THE CLIENT.
    	*/
    	//------------------------------------------------------------------------------
    	//GET THE MESSAGE FROM THE FRAMER
        System.out.print("Receiving...");
        OcpPDU mostRecentAnswer = null;
        byte[] rtnMsg;
        try {
            rtnMsg = framer.nextMsg();
            mostRecentAnswer = coder.fromWire(rtnMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //Message should have message number curMsgNum+1 and code 20
        if(mostRecentAnswer.getStatusCode() != OcpConstants.SC_FILE_REQUEST 
                      || mostRecentAnswer.getMsgNumber() != (curMsgNum+1)){
            System.out.println
                    ("Invalid Status Code or Message Number");
            System.out.println("Received!\n"
                    + "The following PDU was received from the client ");
            System.out.println(mostRecentAnswer.pduToString());
            
            //Create the "Connection Killed" Response
            OcpPDU response = OcpConstants.makeTerminateConnectionNow();
        try {
            byte[] encodedMsg = coder.toWire(response);
            framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{ //We're going to send the file(s) requested back

            curMsgNum = curMsgNum+2; //+1 from client +1 to send back
            
            System.out.println("Received!\n"
                    + "The following PDU was received from the client ");
            System.out.println(mostRecentAnswer.pduToString());

            OcpPDU response = mostRecentAnswer;
            response.setMsgNumber(curMsgNum);  
            response.setDateTime(System.currentTimeMillis());
            response.setStatusCode(OcpConstants.SC_FILE_RESPONSE);

            //Send the "Sending message" PDU
            byte[] encodedMsg;
            try {
                encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Get the URI --- Going to be a specific file name
            String URI = mostRecentAnswer.getDataAsString();
            String[] URIList = URI.split("&&&&");
            File myFile;
            byte[] mybytearray;
            
            try {
				dos.writeInt(URIList.length);
				dos.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
            for( int i = 0; i < URIList.length; i++)
            {
            	try {
            		myFile = new File(serverfileLoc + URIList[i]);
            		long length = myFile.length();
            		dos.writeLong(length);
            		dos.flush();

            		String name = myFile.getName();
            		dos.writeUTF(name);
            		System.out.println("Wrote " + name);
            		dos.flush();

            		FileInputStream fis = new FileInputStream(myFile);
            		BufferedInputStream bis = new BufferedInputStream(fis);

            		int theByte = 0;
            		while((theByte = bis.read()) != -1) {
            			bos.write(theByte);
            		}

            		bos.flush();
	                bis.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        System.out.println("File(s) sent");
        ConfirmClientReceive();
    	
    }
    
    
    private void ConfirmClientAuth(){
    	//------------CONFIRM THE CLIENT (STATE 3-2)---------------------------------------
    	/*
    	        THIS STATE CONFIRMS THAT A CONNECTION HAS BEEN ESTABLISHED WITH THE CLIENT,
    	        FROM WHICH THE CLIENT MAY ASK FOR HIGHLIGHT FILES.
    	*/
    	//------------------------------------------------------------------------------
    	//GET THE MESSAGE FROM THE FRAMER
        System.out.print("Receiving...");
        OcpPDU mostRecentAnswer = null;
        byte[] rtnMsg;
        try {
            rtnMsg = framer.nextMsg();
            mostRecentAnswer = coder.fromWire(rtnMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }
		
        //Here we should have received a 4 status code with message number 
        // curMsgNum+1
        if(mostRecentAnswer.getStatusCode() != OcpConstants.SC_TCP_GTG || 
                    mostRecentAnswer.getMsgNumber() != (curMsgNum+1)){
            System.out.println
                        ("Invalid Status Code or Message Number");
            System.out.println("Received!\n The following PDU was received"
                        + " from the client ");
            System.out.println(mostRecentAnswer.pduToString());
            //Create the "Connection Killed" Response
            OcpPDU response = OcpConstants.makeTerminateConnectionNow();
            response.setDateTime(System.currentTimeMillis());
            try {
                byte[] encodedMsg = coder.toWire(response);
                    framer.frameMsg(encodedMsg, out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }else{ //Valid Response
            System.out.println("Received!\n The following PDU was received"
                        + " from the client ");
            System.out.println(mostRecentAnswer.pduToString());
        
            //Create the message number the client should use.
            curMsgNum = curMsgNum+2;//+1 from client +1 to send back
			
			
			
            OcpPDU response = mostRecentAnswer;
            response.setMsgNumber(curMsgNum);  
            response.setDateTime(System.currentTimeMillis());
            byte[] encodedMsg;
            try {
                encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }

            handleClientRequest();
        }
		
    }
    
    private void HandleClientAuth(){
    	//------------HANDLING CLIENT AUTHENTICATION (STATE 3-1)---------------------------------------
    	/*
    	        THIS STATE TAKES THE INITIAL CONTACT FROM THE CLIENT AND TAKES THE FIRST STEP
    	        IN CONFIRMING THAT THE CLIENT IS AUTHORIZED.
    	*/
    	//------------------------------------------------------------------------------
    
    	Random r = new Random();
        //GET THE MESSAGE FROM THE FRAMER
        System.out.print("Receiving...");
        OcpPDU mostRecentAnswer = null;
        byte[] rtnMsg;
        try {
            rtnMsg = framer.nextMsg();
            mostRecentAnswer = coder.fromWire(rtnMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //The Status code should only be 2. 
        // Anything else and we kill the connection.
        if(mostRecentAnswer.getStatusCode() != OcpConstants.SC_VERS_NEGOTIATE){
            System.out.println("Invalid Status Code");

            //Create the "Connection Killed" Response
            OcpPDU response = OcpConstants.makeTerminateConnectionNow();
            response.setDateTime(System.currentTimeMillis());
            try {
                byte[] encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
                //DECODE THE MESSAGE                
                              
            System.out.println("Received!\n"
                        + "The following PDU was received from the client ");
            System.out.println(mostRecentAnswer.pduToString());
            
            
            //Create the message number the client should use.
            curMsgNum = r.nextInt();
            OcpPDU response = mostRecentAnswer;
            response.setMsgNumber(curMsgNum);
            response.setDateTime(System.currentTimeMillis());
            byte[] encodedMsg;
            try {
                encodedMsg = coder.toWire(response);
                framer.frameMsg(encodedMsg, out);
            } catch (IOException e) {
               e.printStackTrace();
            }

            ConfirmClientAuth();
            
        }
        long time = System.currentTimeMillis();
        System.out.println("Server processed request at : " + time +"\n");
    }
    
    private static void shutDown(){
    	try {
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
}
    public boolean serverHasHighlightWithTags(String[] tags){
        return true; // for now.
    }
    
    public void run() {
        switch (this.tlProtocol){
            case "UDP":
                System.out.println("I am a UDP thread.");
                byte[] encodedMsg = Arrays.copyOfRange(this.packet.getData(),
                        0, this.packet.getLength());
                try {
                OcpMsgCoder coder = new OcpMsgCoder();
                OcpPDU pduUDP = coder.fromWire(encodedMsg);
                System.out.println("The following PDU was received");
                System.out.println(pduUDP.pduToString());
                String[] tags = pduUDP.getTags();
                if(this.serverHasHighlightWithTags(tags)){
                	//return PDU stating that this server is where to
                	//go for the file transfer.
                	System.out.println("That highlight exists here.");
                	//System.out.println("Returning the following PDU");
                	pduUDP.setDateTime(OcpConstants.getDateTime());
                	pduUDP.setMsgNumber(pduUDP.getMsgNumber()+2);
                	pduUDP.setStatusCode(OcpConstants.SC_POS_RESPONSE);
                	pduUDP.setData(new String("test.txt").getBytes(StandardCharsets.UTF_8));
                	//System.out.println(pduUDP.pduToString());
                	//it doesn't actually send back yet.
                	try{
                		System.out.print("Encoding message...");        
                		//coder = new OcpMsgCoder();
                		encodedMsg = coder.toWire(pduUDP);
                		System.out.println("success.");

                		System.out.print("Framing message...");
                		ByteArrayOutputStream baOut = 
                				new ByteArrayOutputStream();
                		OcpFramer udpFramer = new OcpFramer(null);
                		udpFramer.frameMsg(encodedMsg, baOut);
                		byte[] framedMsg = baOut.toByteArray();
                		this.packet.setData(framedMsg);
                		//DatagramPacket dp1 = new DatagramPacket
                		//        (framedMsg, 0,framedMsg.length,
                		//this.clientSocketUDP.getRemoteSocketAddress());

                		System.out.print("Sending message...");
                		this.clientDatagramSocket.send(this.packet);
                		System.out.println("Success.");
                		System.out.println("The following PDU was sent "
                				+ "back to the client (via UDP): ");
                		System.out.println(pduUDP.pduToString());
                	}catch(IOException ioe){
                		System.err.println
                         	("Error in server response. " + 
                                     	ioe.getMessage());
                	}
                	return;
                }else{
                	//create thread to become client and search for tags
                	// to other servers....
                	//then return a PDU with the name of that other 
                	// server and the original tags.
                	System.out.println("No highlight here." +
                			"  Have to go find it");
                }
                }
                catch(Exception e)
                {
                	e.printStackTrace();
                }
                return;  
            case "TCP":
                System.out.println("I am a TCP thread.");
                try {
                    in  = clientSocket.getInputStream();
                    out = clientSocket.getOutputStream();
                    bos = new BufferedOutputStream(out);
                    dos = new DataOutputStream(bos);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                framer = new OcpFramer(in);
                coder = new OcpMsgCoder();

                HandleClientAuth();

                shutDown();
                break;
            default :
                System.out.println("Can't run the " + this.tlProtocol +
                                " mystery server.");
        }
    }
}