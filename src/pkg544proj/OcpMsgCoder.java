/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg544proj;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpMsgCoder class
* This class handles the transformation of the PDU from bytes read in from
* the framer (or the inputstream) to a data structured PDU that is able to be printed
* to console.
*/
public class OcpMsgCoder {
    public static final int MIN_WIRE_LENGTH = 8;
    //public static final int MAX_WIRE_LENGTH = 64;/don't think we'll need this
    
    public byte[] toWire(OcpPDU pdu) throws IOException {
        
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(byteStream);
        
        int statusCode = pdu.getStatusCode();
        short versionNumber = pdu.getVersionNumber();
        short numAnswers = pdu.getNumAnswers();
        int msgNumber = pdu.getMsgNumber();
        short numURI = pdu.getNumURI();
        short numTags = pdu.getNumTags();
        long flags = pdu.getFlags(); //for now just treat flags as a long...
                                        // later we'll have to parse the bits.
        long dateTime = pdu.getDateTime();
        String[] tags = pdu.getTags();
        byte[] data = pdu.getData();
        
        out.writeInt(statusCode);
        out.writeShort(versionNumber);
        out.writeShort(numAnswers);
        out.writeInt(msgNumber);
        out.writeShort(numURI);
        out.writeShort(numTags);
        out.writeLong(flags);
        out.writeLong(dateTime);
        for (String t : tags){
            out.writeUTF(t);
        }
        out.write(data, 0, data.length);
        out.flush();
        
        byte[] pduAsByteArray = byteStream.toByteArray();
        return pduAsByteArray;
    }


 //FUNCTION THAT CHECKS WHETHER THE PASSED BYTE ARRAY IS THE MESSAGE DELIMITER 
    private Boolean isDelimiter(byte[] b){
        if (b.length != OcpConstants.OCP_DELIMITER.length){
            return false;
        }
        for (int i =0; i < b.length; i++){
            if (b[i] != OcpConstants.OCP_DELIMITER[i]){
                return false;
            }
        }
        return true;
    }
//FUNCTION THAT MOVES ALL THE CELLS IN A BYTE ARRAY LEFT
    private byte[] shiftArrayLeft(byte[] b){
        if (b == null) return null;
        for(int i = 0; i<b.length-1; i++){
            b[i] = b[i+1];            
        }
        return b;
    }
 
// RETURN A PDU PER OCP PROTOCOL GIVEN A BYTE ARRAY
    public OcpPDU fromWire(byte[] input) throws IOException{
// MESSAGE HAS TO BE MIN SIZE
        if(input.length < OcpConstants.OCP_MIN_MSG_SIZE){
            throw new IOException("Message doesn't meet min size");
        }
//INITIALIZE INPUT STREAMS        
        ByteArrayInputStream bs = new ByteArrayInputStream(input);
        DataInputStream in = new DataInputStream(bs);
//READ IN ALL THE FIELDS OF THE PDU FROM       
        int statusCode = in.readInt();
        short versionNumber = in.readShort();
        short numAnswers = in.readShort();
        int msgNumber = in.readInt();
        short numURI = in.readShort();
        short numTags = in.readShort();
        long flags = in.readLong();
        long dateTime = in.readLong();
        String[] tags = new String[numTags];
        for (int i = 0; i< numTags; i++){
            tags[i] = in.readUTF();
        }
//INITIALIZE BUFFER TO STORE DATA AS WE'RE READIING IT IN
//  INITIALLY, DON'T KNOW HOW MUCH DATA THERE WILL BE, SO WE 
//  READ UNTIL WE SEE DELIMITER.
        ByteArrayOutputStream dataBuffer = new ByteArrayOutputStream();
 
//NEXT8 IS BYTE ARRAY HOLDING THE NEXT BYTES THAT COULD BE THE DELIMITE        
        byte[] next8 = new byte[OcpConstants.OCP_DELIMITER_SIZE];
//READ IN THE FIRST BYTES
        int bytesRead = in.read(next8, 0, OcpConstants.OCP_DELIMITER_SIZE);
//CONTINUE UNTIL WE SEE THE DELIMITER        
        while (!isDelimiter(next8)){
//READ ONE BYTE AT A TIME FROM THE BYTE ARRAY, WRITE IT TO THE DATA BUFFER
// AND THEN SHIFT THE NEXT BYTE INTO THE BYTE ARRAY
            int nextByte = next8[0];
            dataBuffer.write(nextByte);
            next8 = shiftArrayLeft(next8);
            int ninth;
            if ((ninth = in.read()) == -1){
                if(!next8.equals(OcpConstants.OCP_DELIMITER)){
                    throw new IOException
                                ("Reached end of file without delimiter.");
                }
            }else{
                next8[next8.length-1] = (byte)ninth;
            }
        }
 // MAKE THE BUFFER INTO AN ARRAY ONCE WE'RE DONE READING.       
        byte[] data = dataBuffer.toByteArray();
 
//RETURN THE PDU WITH ALL THE FIELDS WE JUST READ.        
        return new OcpPDU(statusCode,versionNumber, numAnswers,
                        msgNumber, numURI, numTags,
                        flags, dateTime,
                        tags, data);
        
    }
    
}
