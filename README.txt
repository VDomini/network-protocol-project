This repository contains code for a network protocol that is outlined in DesignDocument.pdf.

This protocol was created as a part of a class at Drexel University and was created by myself, Jeromy Guinther, Frederick Montgomery, and Hao Chang.