/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg544proj;

import java.nio.charset.StandardCharsets;

/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpPDU class
* This is the object class for the PDU, containing the constructor and all relevant
* functions to its creation and data manipulation.
*/
public class OcpPDU {
    private int statusCode = OcpConstants.FALSE_STATUS_CODE;
    private short versionNumber = OcpConstants.OCP_ERROR_SHORT;
    private short numAnswers = OcpConstants.OCP_ERROR_SHORT;
    private int msgNumber = OcpConstants.OCP_ERROR_INT;
    private short numURI = OcpConstants.OCP_ERROR_SHORT;
    private short numTags = OcpConstants.OCP_ERROR_SHORT;
    private long flags = 0;
    private long dateTime = 0;
    private String[] tags = null;
    private byte[] data = null;
    
    public OcpPDU(
 // FIELDS ARE IN ACCORDANCE WITH OCP DESIGN PAPER
                int statusCode,
                short versionNumber,
                short numAnswers,
                int msgNumber,
                short numURI,
                short numTags,
                long flags,
                long dateTime,
                String[] tags,
                byte[] data
                ){
        this.statusCode = statusCode;
        this.versionNumber = versionNumber;
        this.numAnswers = numAnswers;
        this.msgNumber = msgNumber;
        this.numURI = numURI;
        this.numTags = numTags;
        this.flags = flags;
        this.dateTime = dateTime;
        try{
            this.tags = tags.clone();
        }catch(NullPointerException npe1){
            this.tags = OcpConstants.OCP_TAG_FAIL;
        }
        try{
            this.data = data.clone();
        }catch(NullPointerException npe2){
            this.data = OcpConstants.OCP_DATA_NONE;
        }
        
        
    }
 
//PROCEDURE TO PRINT THE FIELDS OF THE PDU.
    public String pduToString(){
         String allTheTags = "";
         for (String t : this.tags){
             allTheTags = allTheTags.concat("<" + t + ">\n");
         }
        
        String whatToPrintForData = "<no data included>";
        if (!(this.data == null)){
            whatToPrintForData = new String(this.data, StandardCharsets.UTF_8);
        }
         
        return 
               "\n" +
               "|---------------|\n"+
               "|---START PDU---|\n" +
               "|---------------|\n"+
               "(-STATUS CODE-)\n" + 
               Integer.toString(this.statusCode) +
               "\n(-VERSION #-)\n" + 
               Short.toString(this.versionNumber) + "\n" +
               "(-# ANSWERS-)\n" +
               Short.toString(this.numAnswers) +
               "\n(-MESSAGE NUMBER-)\n" + 
               Integer.toString(this.msgNumber) + "\n" +
               "(-# URI's-)\n" +
               Short.toString(this.numURI) + "\n" +
               "(-# TAGS-)\n" +
               Short.toString(this.numTags) + 
               "\n(-FLAGS-)\n"        +
               Long.toString(this.flags) +
               "\n(-DATE/TIME STAMP-) \n" +
               Long.toString(this.dateTime) +
               "\n(-TAGS-) \n"+
               allTheTags +
               "(-DATA-) \n" +
               whatToPrintForData + "\n" +
               "|-------------|\n"+
               "|---END PDU---|\n" +
               "|-------------|"+
               "\n";
    }
    
    public int getStatusCode(){
        return this.statusCode;
    }
    
    public void setStatusCode(int stat){
    	this.statusCode = stat;
    }
    
    public short getVersionNumber(){
        return this.versionNumber;
    }
    
    public short getNumAnswers(){
        return this.numAnswers;
    }
    
    public int getMsgNumber(){
        return this.msgNumber;
    }
    
    public void setMsgNumber(int num){
        this.msgNumber = num;
    }
    
    public short getNumURI(){
        return this.numURI;
    }
    
    public short getNumTags(){
        return this.numTags;
    }
    
    public void setNumTags(short i){
    	this.numTags = i;
    }
    
    public long getFlags(){
        return this.flags; // treating this as a long only temporarily.
    }
    
    public long getDateTime(){
        return this.dateTime;
    }
    
    public void setDateTime(long t){
        this.dateTime = t;
    }

    public String[] getTags(){
        return this.tags;
    }
    
    public void setTags(String[] tgs){
    	this.tags = tgs;
    }
    
    public byte[] getData(){
        return this.data;
    }
    
    public void setData(byte[] b){
    	this.data = b;
    }
    
    public String getDataAsString(){
    	return new String(this.data, StandardCharsets.UTF_8);
    }
    
    public boolean isLegitMessage(int statusCode, int versionNumber){
        if(statusCode != this.statusCode)
            return false;
        else if(versionNumber != this.versionNumber)
            return false;
       else
            return true;
    }
    
    public boolean isLegitMessage(int statusCode, int versionNumber,
                                    int msgNumber){
        if(statusCode != this.statusCode)
            return false;
        else if(versionNumber != this.versionNumber)
            return false;
        else if(msgNumber != this.msgNumber)
            return false;
        else
            return true;
    }
    
}
