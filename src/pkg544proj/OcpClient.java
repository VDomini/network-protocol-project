/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg544proj;

import java.net.Socket;
import java.net.InetAddress;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
//import java.net.SocketException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
//import java.io.ByteArrayOutputStream;
//import java.io.DataOutputStream;

/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpClient class
* This is the class which contains all of the relevant code for the client.
* It begins by creating the client object, and then calling through the different
* functions which represent the various states of the protocol, and handles the 
* different events which can happen at each.
* 
* This class also contains some useful functions for sending over TCP and UDP, which
* save a lot of repetitive coding.
*/
public class OcpClient {
    
    private int version = OcpConstants.HIGHEST_CLIENT_VERSION;
    private int state;
    //Presumably the first server we're passed on initialization
    private String initialServerName;
    InetAddress initialServerAddress;
    InetAddress nextServerAddress;
    Socket initialSocket;
    //Possibly the next server we're told has our hl's
    private String nextServerName; 
    Socket nextSocket;
    DatagramSocket nextDatagramSocket;
    private String answer;  // Not sure what to use this for yet.
    private String[] tags; //The tags we're looking for (and hopefully rcvg
    private OcpFramer framer; //class to frame and deframe msgs to/from socket
    private OcpMsgCoder coder; //class to encode/decode PDU to/from bytes
    private static final int SILENT = 1;
    private static final int WAIT_FOR_RESPONSE = 2;
    private static final int NEGOTIATE_VERSION = 31;
    private static final int VERSION_CONFIRMED = 32;
    private static final int ESTABLISHED = 4;
    private static final int RECEIVING_TRANSFER = 5;
    private static final int ENDING = 6;
    private static final int FAIL = -1;
    private static final int SUCCESS = 1;  
    private static final String SAVEDEST = "Highlights" + OcpConstants.getOSFileSep();
    public OcpPDU mostRecentAnswer = null;
    public OcpPDU mostRecentSent = null; 
    public int randomNumberToVerify = 0;
    private static InputStream in = null;
    private static OutputStream out = null;
    private static InputStream inUDP = null;
    private static OutputStream outUDP = null;
    private static BufferedInputStream bis = null;
    private static DataInputStream dis = null;
    private int currentMsgNumber = 0;

//-----------------------------------------------------------------------------
    //prints current state client is in.
    public void printState(){
        System.out.println("--------------");
        System.out.println("---STATE " + this.state + "----");
        System.out.println("--------------");
    }
//-----------------------------------------------------------------------------
    
//-----------------------------------------------------------------------------
    //creates a new client with the server names and tags specified
    public OcpClient(String serverName, String[] tags) {

        if (tags.length <1) {
            System.out.println("ERROR IN CREATING CLIENT!");
        }
        this.state = SILENT; // state in state 1, silent
        this.initialServerName = serverName;
        this.nextServerName = "";
        this.tags = tags.clone();
        this.executeSILENT();
    }
//------------------------------------------------------------------------------
    private int getRandomNumber(){
        Random r = new Random();
        return r.nextInt();
    }
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------    
    //SENDS THE NEXT MESSAGE FROM THE CLIENT TO THE SERVER.
    private void sendMostRecent(){
        try{
            //System.out.println("Sending the following PDU...");            

//ENCODE THE PDU INTO BYTES
            System.out.print("Encoding message...");
            this.coder = new OcpMsgCoder();
            byte[] encodedMsg = this.coder.toWire(this.mostRecentSent);
            System.out.println("Success."); 
            
            //FRAME THE PDU INTO A MESSAGE AND SEND.
            System.out.print("Framing and sending message...");
            this.framer = new OcpFramer(in);
            this.framer.frameMsg(encodedMsg, out);
            System.out.println("Success.");
            System.out.println
                        ("The following PDU was sent to the server(via TCP): ");
            System.out.println(this.mostRecentSent.pduToString());
        }catch(IOException ioe){
                System.err.println("Error in server response. " + 
                                    ioe.getMessage());
                return;
        }
    }
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
    //RECEIVES THE NEXT MESSAGE FROM THE SERVER TO THE CLIENT
    private void receiveMostRecent(){
        try{
            //GET THE NEXT MESSAGE FROM THE FRAMER
            System.out.print("Receiving...");
            byte[] rtnMsg = this.framer.nextMsg();
            //DECODE THE MESSAGE                
            this.mostRecentAnswer = this.coder.fromWire(rtnMsg);
            //DISPLAY THE MESSAGE THAT WAS RETURNED 
            System.out.println("Received!\n"
            + "The following PDU was received back from the server(via TCP): ");
            System.out.println(this.mostRecentAnswer.pduToString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
    private void sendUdpMostRecent(){
        try{
            
            System.out.print("Encoding message...");        
            this.coder = new OcpMsgCoder();
            byte[] encodedMsg = this.coder.toWire(this.mostRecentSent);
            System.out.println("success.");
            
            System.out.print("Framing message...");
            ByteArrayOutputStream baOut = new ByteArrayOutputStream();
            OcpFramer udpFramer = new OcpFramer(null);
            udpFramer.frameMsg(encodedMsg, baOut);
            byte[] framedMsg = baOut.toByteArray();
            DatagramPacket dp = new DatagramPacket
                    (framedMsg, 0,framedMsg.length, this.initialServerAddress,
                            OcpConstants.OCP_PORT_PLUS_1);
            
            System.out.print("Sending message...");
            nextDatagramSocket.send(dp);
            System.out.println("Success.");
            System.out.println("The following PDU was sent to the server (via UDP): ");
            System.out.println(this.mostRecentSent.pduToString());
            
        }catch(Exception e){
            e.printStackTrace();
            return;
        }
        
    }
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------    
    public void executeSILENT() throws IllegalStateException{
//------------SILENT (STATE 1)--------------------------------------------------
/*
        ASSUMING WE'VE STARTING HERE BECAUSE AN APPLICATION CALLED US
        WITH THE ARG'S ABOVE PUTTING US INTO STATE 1
        OR WE HAVE ALREADY GOTTEN AN ANSWER BACK FROM OUR UDP REQUEST 
        (WE'VE COME BACK FROM STATE 2).  OR WE'VE BACKTRACKED HERE FROM STATE
        3-2 BECAUSE THERE WAS AN AUTH. FAILURE.
        
        TRANSITION TO STATE 2 BY SENDING A UDP REQUEST TO THE SERVER WE KNOW 
        ABOUT (FROM THE ARGS) ASKING FOR SERVER LOCATION THAT HAS TAGS WE WANT.
        
        TRANSITION TO STATE 3-1 BY SENDING A VERSION REQUEST MESSAGE.
*/
//------------------------------------------------------------------------------
        if (this.state != SILENT){
            throw new IllegalStateException
                            ("Attempt to execute in wrong state.");    
        }
        this.printState();
                
        if (nextServerName.equals("")){ //we don't have the UDP query back yet...

            // (INSERT DESCRIPTION OF WHY WE'LL ALWAYS START W/ UDP MESSAGE.
            this.currentMsgNumber = getRandomNumber();
            this.mostRecentSent = OcpConstants.makeClientHighlightRequest();
            this.mostRecentSent.setMsgNumber(this.currentMsgNumber);
            this.mostRecentSent.setTags(tags);
            this.mostRecentSent.setNumTags((short)tags.length);
            
            try{
                this.nextDatagramSocket = new DatagramSocket();
                this.initialServerAddress = InetAddress.getByName
                                            (this.initialServerName);
                System.out.print("Connecting DatagramSocket...");
                this.nextDatagramSocket.connect(
                        this.initialServerAddress,OcpConstants.OCP_PORT_PLUS_1);
                this.nextDatagramSocket.setSoTimeout(OcpConstants.UDP_TIMEOUT);
                System.out.println("complete" + OcpConstants.OCP_PORT_PLUS_1);
            }catch(Exception e){
                System.out.println("Problem sending UDP in State 1");
                return;
            }
            sendUdpMostRecent();
            
            
            this.state = WAIT_FOR_RESPONSE; //state 2
            this.executeWAIT_FOR_RESPONSE();
        }else if(!nextServerName.equals(initialServerName)){
            //we have to go to a different server
            // we'll reset and start over at that server.
            // this could be a challenge with passwords.. Maybe delete pw req.
            initialServerName = nextServerName;
            nextServerName = "";
            this.state = SILENT;
            this.executeSILENT();            
        }else{//this server is the one to go to..
            
            try{
                //Create a socket that is connected to server on OCP's port #
                this.nextSocket = new Socket
                                    (nextServerName, OcpConstants.OCP_PORT);
                //If the line above fails, an IOException is thrown.

                //SET THE INPUT AND OUTPUT STREAMS     
                in = this.nextSocket.getInputStream();
                out = this.nextSocket.getOutputStream();
                bis = new BufferedInputStream(in);
                dis = new DataInputStream(in);
            }catch(IOException ioe){
                    System.err.println("Error in server response. " + 
                                        ioe.getMessage());
                    return;
            }
            System.out.println("TCP connection established to server...");
            
            this.randomNumberToVerify = getRandomNumber();
            this.mostRecentSent = OcpConstants.makeClientVersionNegotiate(
                                                    this.randomNumberToVerify);
            this.mostRecentSent.setDateTime(OcpConstants.getDateTime());
            this.mostRecentSent.setTags(tags);
            this.mostRecentSent.setNumTags((short)tags.length);
            //this.mostRecentSent.setMsgNumber(getRandomNumber());            
            //                    
            //currently we implement this with 0 as the first msg number
            // from client to server, then server randomizes it for UDP...why?
            this.sendMostRecent();
            
            this.state = NEGOTIATE_VERSION;
            this.executeStateNEGOTIATE_VERSION();
        }
    }

//------------------------------------------------------------------------------    
    public void executeWAIT_FOR_RESPONSE() throws IllegalStateException{
//------------WAIT FOR RESPONSE (STATE 2)---------------------------------------
/*
        WE'RE HERE BECAUSE WE'VE SENT A UDP REQUEST TO A SERVER AND WE'RE
        WAITING FOR THE RESPONSE.
        
        FOR TESTING, INITIALLY JUST MAKE THIS RETURN THE SAME SERVER WE KNOW 
        ABOUT THEN TEST WHEN THE SERVER RETURNS ANOTHER SERVER THAT HAS THE TAGS
        
        TRANSITION BACK TO STATE 1 BY PARSING THE RESPONSE RETURNED FROM THE
        UDP SERVER (THIS INCLUDES PROCESSING THE PUBLIC KEY THAT WAS RETURNED)
        
        IF A TIMEOUT IS RECEIVED FROM THE SERVER, RESEND THE REQUEST AND STAY
        IN THIS STATE.
*/
//------------------------------------------------------------------------------
        if (this.state != WAIT_FOR_RESPONSE){
            throw new IllegalStateException
                            ("Attempt to execute in wrong state.");  
        }
        this.printState();
        boolean receivedResponse = false;
        int tries= 0;
        DatagramPacket dp = new DatagramPacket(
                        new byte[OcpConstants.MAX_UDP_SIZE], 
                        OcpConstants.MAX_UDP_SIZE);
        System.out.print("Receiving UDP Response...");
        do {
            try{
                this.nextDatagramSocket.receive(dp);
                if (!dp.getAddress().equals(this.initialServerAddress)){
                    System.out.println("Ignoring request from unknown location.");
                    return;
                }
                receivedResponse = true;
            }catch(IOException e){
                tries+= 1;
                System.out.println("Timeout w/" + 
                        (OcpConstants.UDP_MAX_TRIES - tries) + " more tries");
                System.out.print("Receiving UDP Response...");
            }
        }while((!receivedResponse) && (tries < OcpConstants.UDP_MAX_TRIES));
        
        if (receivedResponse){
            System.out.println("Received!"); 
            byte[] b = dp.getData();
            try{
                this.mostRecentAnswer = this.coder.fromWire(b);
            }catch(Exception e){
                System.err.println("Problem decoding UDP message.");
            }
            System.out.println("The following PDU was received (via UDP)\n" +
                        this.mostRecentAnswer.pduToString());
        }else{
            System.out.println("Unable to receive response back. Giving up.");
        }
 
        
        //System.out.println("UDP Request would be received here.");
        //this.mostRecentAnswer = OcpConstants.makePositiveResponse();
       // System.out.println("Assume we received the following PDU (from UDP):");
        //System.out.println(this.mostRecentAnswer.pduToString());
        
        /*STILL WORKING ON THIS.....
        //should get a positive response or negative response back.
        // anything else is whack.
        if(!mostRecentAnswer.isLegitMessage(OcpConstants.SC_POS_RESPONSE,
                                            this.version,
                                            this.currentMsgNumber +1)){
            if(!mostRecentAnswer.isLegitMessage(OcpConstants.SC_NEG_RESPONSE,
                                            this.version,
                                            this.currentMsgNumber+1)){
                System.out.println("Illegitimate UDP response ignored.");
                this.executeWAIT_FOR_RESPONSE();
                return;
            }
        }
        */
        
        this.currentMsgNumber = this.currentMsgNumber + 2;
        //Really, we're going to have to parse out next server name here.
        // for now, its' hard coded.
        this.nextServerName = new String(this.initialServerName);
        
        this.state = SILENT; //go back to state 1
        this.executeSILENT();
    }
//------------------------------------------------------------------------------    
    public void executeStateNEGOTIATE_VERSION() throws IllegalStateException{
//------------NEGOTIATE VERSION (STATE 3-1)-------------------------------------
/*
        WE'RE HERE BECAUSE WE KNOW ABOUT A SERVER THAT HAS THE HIGHLIGHTS WE 
        WANT (PRESUMABLY FROM MOVING HERE FROM STATE 1 AFTER STATE 1 FOUND
        A SERVER VIA ITS UDP REQUEST AND HAS SENT A VERSION REQUEST TO THE SRVR)
        
        TRANSITION TO STATE 4 BY RECEIVING AND PARSING MESSAGE BACK FROM SERVER
        (THIS INCLUDES GEN'G A RANDOM NUMBER AND ENCRYPTING IT W/ THE
        SERVER'S PUBLIC KEY AND SENDING IT BACK TO SERVER)
        
*/
//----------------------------------------------------------------------------- 
        if (this.state != NEGOTIATE_VERSION){
            throw new IllegalStateException
                            ("Attempt to execute in wrong state.");  
        }
        this.printState();


        
        //System.out.println("Server's Request would be received here.");
        //this.mostRecentAnswer = OcpConstants.makeServerVersionResponse
        //                                        (this.randomNumberToVerify);
        //System.out.println("Assume we received the following PDU "
        //        + "(auth / version ) :");
        //System.out.println(this.mostRecentAnswer.pduToString());
        
        this.receiveMostRecent();
        //System.out.println("Because the above message is still just an echo..." 
        //        + "Pretend this message was received: ");
        this.mostRecentSent = this.mostRecentAnswer;
        this.mostRecentSent.setMsgNumber(this.mostRecentAnswer.getMsgNumber()+1);
        this.mostRecentSent.setStatusCode(OcpConstants.SC_TCP_GTG);
        this.mostRecentSent.setDateTime(System.currentTimeMillis());
        
        this.sendMostRecent();
        //the above line should end up in a good verification every time
        //  since we get a reply back with the same random number we sent.
        //this.mostRecentAnswer = OcpConstants.makeServerVersionResponse(69);
        //the above line should always end up in a failed verfication.
        
        
        this.state = VERSION_CONFIRMED;
        this.executeVERSION_CONFIRMED();
    }

    private int getIntFromData(){
        byte[] data = this.mostRecentAnswer.getData();
        
        //System.out.println("Working on finding that data for you.");
        
        //System.out.println("Data size " + data.length + " bytes.");
        
        return java.nio.ByteBuffer.wrap(data).getInt();
        
        //return 69;
    }
    
    private boolean randomResponseChecksOut(){
        
        //System.out.println("Let's see if the random response checks out...");
        int x = getIntFromData();
        //System.out.println("Here's what I think the number received is : "+
          //                  x);
        //System.out.println("Here's what I think the original was : " +
          //                  this.randomNumberToVerify);
        
        boolean areTheyEqual = x == this.randomNumberToVerify;
        //System.out.println(areTheyEqual);
                 
        //return true;
        return areTheyEqual;
    }
    
    private int setHighestVersion(){
        return OcpConstants.HIGHEST_CLIENT_VERSION; // right now just returns
        // the highest version we can do.  Needs to parse pdu and figure out
        // how what client can do compares to what server can do.
    }
    
//------------------------------------------------------------------------------    
    public void executeVERSION_CONFIRMED() throws IllegalStateException{
//------------[VERSION CONFIRMED ] (STATE 3-2)----------------------------------
/*
        WE'RE HERE BECAUSE WE'VE RECEIVED BACK AUTHENTICATION FROM A SERVER
        AND THE VERSION THAT ITS CAPABLE OF SUPPORTING.  
        
        TRANSITION BACK TO STATE 1 IF THE SERVER AUTHENTICATION FAILED.
        TRANSITION TO STATE 4 IF THE AUTHENTICATION WAS GOOD BY
        SENDING A TCP CONNECT TO THE SERVER USING THE
        CORRECT VERSION THAT THE SERVER CAN SUPPORT.
*/
//-----------------------------------------------------------------------------
    	this.receiveMostRecent();
    	
    	
    	if (this.state != VERSION_CONFIRMED){
            throw new IllegalStateException
                        ("Attempt to execute in wrong state.");  
        }
        this.printState();
        //insert code to check whether server verification passes or not
        //  insert code to set version correctly, based on server response.
        // for now just set highest client version available.
        if (!(this.randomResponseChecksOut())){
            System.out.println("Server Authentication Failed.");
            System.out.println("Check your Initial Server is legitimate"
                    + "  and try again.");
            //return to end this request since the server failed.
            this.state = ENDING;
            this.executeENDING();
            return;
        }
        
        
        
        this.version = this.setHighestVersion();
        System.out.println("Server Authentication Good to Go...");
        System.out.println("Using version " + this.version);
        
       /* this.receiveMostRecent();
        this.mostRecentSent = this.mostRecentAnswer;
        this.mostRecentSent.setMsgNumber(this.mostRecentAnswer.getMsgNumber()+1);
        this.mostRecentSent.setDateTime(System.currentTimeMillis());
        this.mostRecentSent.setStatusCode(OcpConstants.SC_TCP_GTG);
        this.sendMostRecent();*/
        
        this.state = ESTABLISHED;
        this.executeESTABLISHED();
    }

//------------------------------------------------------------------------------
    public void executeESTABLISHED() throws IllegalStateException{
//------------[ ESTABLISHED ]  (STATE 4)----------------------------------------
/*
        WE'RE HERE BECAUSE WE ARE CONNECTED TO A SERVER, WITH VERSION INFO
        AND AUTHENTICATION COMPLETE. WE KNOW THIS SERVER HAS OUR HIGHLIGHTS
        
        TRANSITION TO STATE 5 BY SENDING URI REQUEST
*/
//----------------------------------------------------------------------------- 
        if (this.state != ESTABLISHED){
            throw new IllegalStateException
                        ("Attempt to execute in wrong state.");  
        }
        this.printState();
        //try{
            //Create a socket that is connected to server on OCP's port #
            //this.nextSocket = new Socket
                                //(nextServerName, OcpConstants.OCP_PORT);
            //If the line above fails, an IOException is thrown.
            
            //SET THE INPUT AND OUTPUT STREAMS     
            //InputStream in = this.nextSocket.getInputStream();
            //OutputStream out = this.nextSocket.getOutputStream();
             
            //System.out.println("Client connected to server..."
            //System.out.println("Sending the following PDU (file request)..");
        
            //BUILD A ARBRITRARY PDU TO TEST SENDING BACK AND FORTH
            // CHANGE THIS LATER TO ACTUALLY SEND THE TAGS WE CARE ABOUT.
            //String[] testTags = {"sixers", "lakers"};
            //OcpPDU testPDU = new OcpPDU(20, (short)1, (short)0, 
            //        69, (short)3, (short)2, 
            //        0, 0, 
            //        testTags, null);
            //System.out.println(testPDU.pduToString());
        
             this.mostRecentSent = OcpConstants.makeClientFileRequest();
             this.mostRecentSent.setMsgNumber(this.mostRecentAnswer.getMsgNumber()+1);
             this.mostRecentSent.setTags(tags);
             this.mostRecentSent.setNumTags((short)tags.length);
             //System.out.println(this.mostRecentSent.pduToString());
            
             this.sendMostRecent();
             /*
            //ENCODE THE PDU INTO BYTES
            System.out.print("Encoding message...");
            this.coder = new OcpMsgCoder();
            System.out.println("Success.");            
            byte[] encodedMsg = this.coder.toWire(this.mostRecentSent);
            
            //FRAME THE PDU INTO A MESSAGE AND SEND.
            System.out.print("Framing and sending message...");
            this.framer = new OcpFramer(this.in);
            System.out.println("Success.");
            this.framer.frameMsg(encodedMsg, this.out);
             */
        //}catch(IOException ioe){
          //      System.err.println("Error in server response. " + 
            //                        ioe.getMessage());
              //  return;
       // }
        
        //GO TO WAITING STATE TO SEE WHAT SERVER SENDS BACK.        
        this.state = RECEIVING_TRANSFER;
        this.executeRECEIVING_TRANSFER();
        
    }

//------------------------------------------------------------------------------
    public void executeRECEIVING_TRANSFER() throws IllegalStateException{
//------------[ RECEIVING TRANSFER ] (STATE 5)----------------------------------
/*
        WE'RE HERE BECAUSE WE'RE WAITING FOR THE FILE WE'VE REQUESTED TO 
        FINISH DOWNLOADING
        
        REMAIN IN THIS STATE IF WE RECEIVE MORE PIECES, BUT NOT YET THE FINISHED
        FILE.
        
        TRANSITION TO STATE 6 IF WE RECEIVE A FILE TRANSFER COMPLETE MESSAGE
        WE COULD ALSO TRANSITION TO STATE 6 EARLY BY STOPPING THE TRANSFER,
        BUT NO APPARENT REASON WHY WE'D WANT TO DO THAT.
*/
//-----------------------------------------------------------------------------
        if (this.state != RECEIVING_TRANSFER){
            throw new IllegalStateException
                        ("Attempt to execute in wrong state.");  
        }
        this.printState();
        //System.out.print("Receiving...");
        this.receiveMostRecent();
        //RECEIVE THE RESPONSE BACK FROM THE SERVER
        /*
        System.out.println("Since the above message is still just an echo... + "
                + "assume we received the following PDU...");
        
        this.mostRecentAnswer = OcpConstants.makeServerFileResponse();
        System.out.println(this.mostRecentAnswer.pduToString());
        */
        //DETERMINE THE NATURE OF THE RESPONSE FROM THE SERVER.
        if(!this.isLegitFileResponse()){
            //IF RESPONSE IS WHACK, RAISE AN ERROR.
            //SEND ERROR MESSAGE BACK TO SERVER AND CLOSE OUT.
        }
        
        //IF THE RESPONSE IS LEGIT.  SAVE IT AND DISPLAY IT
        this.saveAndDisplayHighlight();
        
        this.state = ENDING;
        this.executeENDING();
    }
 
    private boolean isLegitFileResponse(){
        return true;//but just for now.
    }
    
    private void saveAndDisplayHighlight(){
    	int filesCount;
    	//DataInputStream dis2 = new DataInputStream(in);
    	//BufferedInputStream bis2 = new BufferedInputStream();
    	
		try {
			filesCount = dis.readInt();
		
			File[] files = new File[filesCount];
			
			for(int i = 0; i < filesCount; i++)
			{
				long fileLength = dis.readLong();
				String fileName = dis.readUTF();

				files[i] = new File(SAVEDEST + fileName);

				FileOutputStream fos = new FileOutputStream(files[i]);
				BufferedOutputStream bos = new BufferedOutputStream(fos);

				for(int j = 0; j < fileLength; j++) 
					bos.write(bis.read());

				bos.flush();
				bos.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    

    
//------------------------------------------------------------------------------    
    public void executeENDING() throws IllegalStateException{
//------------[ ENDING ] (STATE 6)----------------------------------------------
/*
        WE'RE HERE BECAUSE FILE TRANSFER IS COMPLETE.  
        
        TRANSITION BACK TO STATE 1 SILENT BY SENDING CLOSE CONFIRM
*/
//----------------------------------------------------------------------------- 
        if (this.state != ENDING){
            throw new IllegalStateException
                        ("Attempt to execute in wrong state.");  
        }    
        
        
        this.printState();
        
        
        this.mostRecentSent = OcpConstants.makeTerminateConnection();
        this.mostRecentSent.setMsgNumber(this.mostRecentAnswer.getMsgNumber()+1);
        this.mostRecentSent.setTags(tags);
        this.mostRecentSent.setNumTags((short)tags.length);
        //System.out.println(this.mostRecentSent.pduToString());
       
        this.sendMostRecent();
        
        this.receiveMostRecent();
        //CLOSE THE CONNECTION            
        try{
            nextSocket.close();
            //Close the socket and its streams
            System.out.println("TCP connection to server closed");
        }catch (Exception e){
                e.printStackTrace();
        }
        
        this.state = SILENT;
        
    }
}
