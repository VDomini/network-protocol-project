/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg544proj;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpConstants class
* This class is more or less a global constants or helper class for the entire
* process. Big shoutout to Jeromy here for putting this together. It contains the
* global message codes as well as functions which set up the PDU for given functional needs.
*/
public class OcpConstants {
    public static final int OCP_PORT = 9069;
    public static final int OCP_PORT_PLUS_1 = 9071;
    public static final int UDP_MAX_TRIES = 10;
    public static final int UDP_TIMEOUT = 500;
    public static final int MAX_UDP_SIZE = 128;
    public static final String DEFAULT_SERVER_NAME = "tux64-13.cs.drexel.edu";
    public static final byte[] DEFAULT_SERVER_ADDRESS = {127, 0,0,1};
    public static final int CLIENT_REQ_FILE = 20;
    public static final int FALSE_STATUS_CODE = 0;
    public static final short OCP_ERROR_SHORT = -1;
    public static final int OCP_ERROR_INT = -1;
    public static final String[] OCP_TAG_FAIL = {"NO TAGS INCLUDED"};
    public static final byte[] OCP_DATA_NONE = "NO DATA".getBytes();
//DELIMITER IS USED TO FRAME MESSAGES.  XXXXXXXX (IN BYTES) IS THE END OF A MES.
    public static final byte[] OCP_DELIMITER = "XXXXXXXX".getBytes();
    public static final int OCP_DELIMITER_SIZE = 8;
    public static final int OCP_MIN_MSG_SIZE = 32;
    public static final short HIGHEST_CLIENT_VERSION = (short)1;
 // THE FOLLOWING ARE THE POSSIBLE STATUS CODES.
    public static final int SC_HIGHLIGHT_REQ_FROM_CLIENT = 1;
    public static final int SC_HIGHLIGHT_REQ_FROM_SERVER = 11;
    public static final int SC_POS_RESPONSE = 111;
    public static final int SC_NEG_RESPONSE = 121;
    public static final int SC_ERR_RESPONSE = 131;
    public static final int SC_VERS_NEGOTIATE  = 2;
    public static final int SC_TCP_GTG = 4;
    public static final int SC_FILE_REQUEST = 20;
    public static final int SC_FILE_RESPONSE = 22;
    public static final int SC_TERMINATE = 40;
    public static final int SC_KILL_NOW = 66;
    public static final int SC_SECURITY_6 = 6;
    public static final int SC_SECURITY_7 = 7;
    public static final int SC_SECURITY_8 = 8;
    
    public static final long UNUSED_FIELD_64 = (long) 0;
    public static final int UNUSED_FIELD_32 = 0;
    public static final short UNUSED_FIELD_16 = (short) 0;
    
    public static int NUM_TEST_TAGS_PLUS1 = 3;
    public static String[] TEST_TAGS = {"sixers", "lakers"};
    public static String[] NO_TAGS = {"NO TAGS"};
    public static byte[] EMPTY_DATA = "NO DATA".getBytes();
    private static String aspk = //128 byte,1024 bit key ~ 32 ints long
            "abcdefghijklmnopqrstuvwxyz123456";
    public static byte[] ARBITRARY_SERVER_PUBLIC_KEY =  aspk.getBytes();
/* 
|-------------------------START PDU------------------------|
(-STATUS CODE-)		(-VERSION #-)	(-# ANSWERS-)
<-----32 bits------>	<--16 bits-->	<--16 bits-->
(-MESSAGE NUMBER-)	(-# URI's-)	(-# TAGS-)
<-----32 bits------>	<--16 bits-->	<--16 bits-->
(-FLAGS-)
<----------64 bits------------>
(-DATE/TIME STAMP-) 
<----------64 bits------------>
(-TAGS-) 
<----------UTF-8 STRING (0)-----------> <-\n->
...
<----------UTF-8 STRING (n-1)---------> <-\n->
    (-DATA-) 
<----------UNLIMITED BYTES--------->>
<"XXXXXXXX">    
|--------------------------END PDU-------------------------|
*/    
    
    
//SOME TEST MESSAGES TO PRETEND WE GOT THESE TO/FROM THE SERVERS/CLIENTS
    //AS WE TEST.
    //THESE ARE NOT YET IMPLEMENTED.  WILL BE SOON.
    
    public static long getDateTime(){
        return System.currentTimeMillis();
    }
    
    public static OcpPDU makePositiveResponse(){
        //111 Positive Response Message
        // server is sending back a "Yes I have that file, you want it?" mess.
       
        byte[] a = {4};//ipv4, would be 16 for ipv6
        byte[] b = DEFAULT_SERVER_ADDRESS;
        byte[] c = {(byte)128};
        byte[] d = ARBITRARY_SERVER_PUBLIC_KEY;
        byte[] e = new byte[a.length + b.length + c.length + d.length];
        System.arraycopy(a, 0, e, 0, a.length);
        System.arraycopy(b, 0, e, a.length, b.length);
        System.arraycopy(c, 0, e, a.length+b.length, c.length);
        System.arraycopy(d, 0, e, a.length+b.length+c.length, d.length);
        return new OcpPDU(
                    SC_POS_RESPONSE, //status code
                    (short) HIGHEST_CLIENT_VERSION, //version number
                    UNUSED_FIELD_16, //number of answers.
                    UNUSED_FIELD_32,//message number
                    UNUSED_FIELD_16, //number of URI's 
                    (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                    UNUSED_FIELD_64, //flags
                    getDateTime(), //date, time
                    new String[]{TEST_TAGS[0],
                         TEST_TAGS[1],
                         "THIS IS AN ARBITRARY POSITIVE RESPONSE"},//tags
                    e);//#bytes of ip address + ip address + 
                        // # bytes of pub key + public key
        
    }
    
    public static OcpPDU makeNegativeResponse(){
        
        //121 Negative Response Message
        // server is sending back a "No, I don't have that file, but I'm
        //   looking for it" message.
        return new OcpPDU(
            SC_NEG_RESPONSE, //status code
            (short) HIGHEST_CLIENT_VERSION, //version #
            UNUSED_FIELD_16, //number of answers.
            UNUSED_FIELD_32,//message number
            UNUSED_FIELD_16, //number of URI's 
            (short)NUM_TEST_TAGS_PLUS1, //number of tags.
            UNUSED_FIELD_64, //flags
            getDateTime(), //date, time
            new String[]{TEST_TAGS[0],
                         TEST_TAGS[1],
                         "THIS IS AN ARBITRARY NEGATIVE RESPONSE"},//tags
            EMPTY_DATA);//data
    }
    
    public static OcpPDU makeErrorResponse(){
        
        //131 Negative Response Message
        // server is sending back a "No, I don't have that file, but and I
        //   don't think anyone does.  Something is wrong with your request."
        return new OcpPDU(
            SC_ERR_RESPONSE, //status code
            (short) HIGHEST_CLIENT_VERSION, //version #
            (short)2, //number of answers.
            UNUSED_FIELD_32,//message number
            UNUSED_FIELD_16, //number of URI's 
            (short)NUM_TEST_TAGS_PLUS1, //number of tags.
            UNUSED_FIELD_64, //flags
            getDateTime(), //date, time
            new String[]{TEST_TAGS[0],
                         TEST_TAGS[1],
                         "THIS IS AN ARBITRARY ERROR RESPONSE"},//tags
            EMPTY_DATA);//data
    }
    
    public static OcpPDU makeClientHighlightRequest(){
        // 1 Client Request for Highlights
        //  The client is asking (presumably over UDP for highlights with the
        //   appropriate tags.
        return new OcpPDU(SC_HIGHLIGHT_REQ_FROM_CLIENT,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY(CLIENT)"
                                              + " HIGHLIGHT REQUEST"},
                         EMPTY_DATA);
    }
    
    public static OcpPDU makeRecursiveHighlightRequest(){
        // 11 Server Request for Highlights (to another server)
        //  The server is asking (presumably over UDP) for highlights with the
        //   appropriate tags.  Presumably, this is a recursive request
        //   from one server to another.
        return new OcpPDU(SC_HIGHLIGHT_REQ_FROM_SERVER,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY(RECURSIVE)"
                                              + " HIGHLIGHT REQUEST"},
                         EMPTY_DATA);
    }
    
    public static OcpPDU makeClientVersionNegotiate(int encryptedNumber){
        // 2 Client message to server to negotiate version
        // One side is telling the other nothing more than the
        //  highest version it is able to use.
        //byte[] b = encryptedNumber;
        
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(encryptedNumber);
        byte[] result = b.array();
        
        return new OcpPDU(SC_VERS_NEGOTIATE,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY"
                                              + " VERSION NEGOTIATION"},
                         result);//data is random #
                                                        //presumably 
                                                        //encrypted with server
                                                        //public key
    }
    
    public static OcpPDU makeServerVersionResponse(int decryptedNumber){
        // 2 Server message to client with version its capable
        // Client is telling server highest version it can use as well as
        //  sending it a random number encryped with the server's public key
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(decryptedNumber);
        byte[] result = b.array();
        
        return new OcpPDU(SC_VERS_NEGOTIATE,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY"
                                              + " VERSION NEGOTIATION"},
                         result); // data is server's
                                                        //decryption of the
                                                        // random number sent
                                                        // by client previous.

    }
 
    public static OcpPDU makeClientFileRequest(){
        // 20 client message requesting file from server.
        // This is client asking for file with tags.  he already knows
        //  server has the file.
    	byte[] d = new String("test.txt").getBytes(StandardCharsets.UTF_8);
        return new OcpPDU(SC_FILE_REQUEST,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY FILE"
                                              + " REQUEST"},
                         d);
    }
    
    public static OcpPDU makeServerFileResponse(){
        // 22 server message including file.
        // server is respondin with file client requested.
        byte[] data = "PUT REAL DATA HERE".getBytes();
        return new OcpPDU(SC_FILE_RESPONSE,//status cod
                          (short) HIGHEST_CLIENT_VERSION,//
                          (short)1,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN ARBITRARY FILE"
                                              + " RESPONSE"},
                          data);

    }
 
    public static OcpPDU makeTerminateConnection(){
        // 40 Message to terminate connection.
        // this is a normal termination message.
        
        return new OcpPDU(SC_TERMINATE,//status code
                          (short) HIGHEST_CLIENT_VERSION,//version number
                          UNUSED_FIELD_16,//number of answers
                          UNUSED_FIELD_32,//message number
                          UNUSED_FIELD_16, //number of URI's 
                          (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                          UNUSED_FIELD_64, //flags
                          getDateTime(),
                          new String[]{TEST_TAGS[0],
                                      TEST_TAGS[1],
                                      "THIS IS AN TERMINATE MESSAGE."},
                         EMPTY_DATA);
    }
    
    public static OcpPDU makeTerminateConnectionNow(){
        // 66 Message to terminate connection immediately
        // this is an abnormal terminate message.
        return new OcpPDU(SC_KILL_NOW,//status code
                  (short) HIGHEST_CLIENT_VERSION,//version number
                  UNUSED_FIELD_16,//number of answers
                  UNUSED_FIELD_32,//message number
                  UNUSED_FIELD_16, //number of URI's 
                  (short)NUM_TEST_TAGS_PLUS1, //number of tags.
                  UNUSED_FIELD_64, //flags
                  getDateTime(),
                  new String[]{TEST_TAGS[0],
                              TEST_TAGS[1],
                              "THIS IS AN KILL IMMEDIATELY MESSAGE."},
                 EMPTY_DATA);
    }
    
    public OcpPDU makeTLSCerts6(){
        // Message to negotiate TLS certs..
        return null;
    }
    
    public OcpPDU makeTLSCerts7(){
        // Message to negotiate TLS certs.
        return null;
    }
    
    public OcpPDU makeTLSCerts8(){
        // Message to negotiate TLS certs.
        return null;
    }
    
    public static String getOSFileSep(){
    	String OS = System.getProperty("os.name").toLowerCase();
    	if(OS.indexOf("win") >= 0){
    		return "\\";
    	}
    	else
    	{
    		return "/";
    	}
    }
    
//-----------------------------------------------------------------------------

}



//------------[ INSERT NAME ] (STATE X)--------------------------------------
/*
        ENTER COMMENTS
*/
//-----------------------------------------------------------------------------