/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpFramer class
* This class serves to handle the receiving or sending of a PDU from a given
* input or outputstream.
*/
package pkg544proj;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
/**
 *
 * @author smooth
 */
public class OcpFramer {
    
    private InputStream in;//data source
    
    public OcpFramer (InputStream in){
        this.in = in;
     }

    
    public void frameMsg(byte[] msg, OutputStream out) throws IOException{
//ENSURE MESSAGE DOES NOT CONTAIN DELIMITER
    int length = OcpConstants.OCP_DELIMITER_SIZE;
          
    for (int offset = 0; offset< msg.length - length; offset++) {
        ByteBuffer bb = ByteBuffer.wrap(msg, offset, length).slice();
        if (bb.compareTo(ByteBuffer.wrap(OcpConstants.OCP_DELIMITER)) == 0){
            throw new IOException("Message cannot contain delimiter, " + 
                                OcpConstants.OCP_DELIMITER);
        }
    }

//WRITE THE MESSAGE, FOLLOWED BY THE DELIMITER.    
    out.write(msg);
    out.write(OcpConstants.OCP_DELIMITER);
    out.flush();
}
//FUNCTION TO MOVE ALL THE CELLS IN A BYTE ARRAY LEFT.
    private byte[] shiftArrayLeft(byte[] b){
        if (b == null) return null;
        for(int i = 0; i<b.length-1; i++){
            b[i] = b[i+1];            
        }
        return b;
    }

//FUNCTION THAT CHECKS WHETHER THE PASSED BYTE ARRAY IS THE MESSAGE DELIMITER
    private Boolean isDelimiter(byte[] b){
        if (b.length != OcpConstants.OCP_DELIMITER.length){
            return false;
        }
        for (int i =0; i < b.length; i++){
            if (b[i] != OcpConstants.OCP_DELIMITER[i]){
                return false;
            }
        }
        return true;
    }
    
    public byte[] nextMsg() throws IOException{
        
        ByteArrayOutputStream msgBuffer = new ByteArrayOutputStream();
        int length = OcpConstants.OCP_DELIMITER_SIZE;
        //int offset = 0;
        if (this.in == null){
            throw new IOException("nextMsg received null input!!");
        }
        
        byte[] next8 = new byte[OcpConstants.OCP_DELIMITER_SIZE];
        int bytesRead = this.in.read(next8, 0, OcpConstants.OCP_DELIMITER_SIZE);
        while (!isDelimiter(next8)){
            //System.out.println(next8.equals(OcpConstants.OCP_DELIMITER));
            int nextByte = next8[0];
            msgBuffer.write(nextByte);
            next8 = shiftArrayLeft(next8);
            int ninth;
            if ((ninth = this.in.read()) == -1){
                if(!next8.equals(OcpConstants.OCP_DELIMITER)){
                    throw new IOException
                                ("Reached end of file without delimiter.");
                }else{
                    return msgBuffer.toByteArray();
                }
            }else{
                next8[next8.length-1] = (byte)ninth;
            }
        }
        msgBuffer.write(next8);
        return msgBuffer.toByteArray();
    }
}