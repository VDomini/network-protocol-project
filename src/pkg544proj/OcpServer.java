package pkg544proj;

import java.io.ByteArrayOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.util.Scanner;
import java.io.IOException;
import java.util.Arrays;

/*
* Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
* 6/3/2016
* OcpServer class
* This class contains the main function for the server code, which serves
* to open up the relevant ports for the UDP and TCP server, and then hand 
* off their handling to a thread which is run under the run() command. Anytime
* a new connection is received, it is sent to WorkerRunnable to be handled.
*/

public class OcpServer implements Runnable{

    protected int          serverPort   = 8080;
    protected ServerSocket serverSocket = null;
    protected DatagramSocket clientSocketUDP = null;
    protected boolean      isStopped    = false;
    protected Thread       runningThread= null;
    protected String       udpOrTcp = "TCP";

    public OcpServer(int port, String udpOrTcp){
        this.serverPort = port;
        this.udpOrTcp = udpOrTcp;
    }
    
    public void run(){
        synchronized(this){
            this.runningThread = Thread.currentThread();
        }
        if (this.udpOrTcp == "TCP"){
            openServerSocket();
            System.out.println("Server Started. Listening on (TCP)Port: " + 
                                        this.serverPort);
        }else if (this.udpOrTcp == "UDP"){
            openDatagramServerSocket();
            System.out.println("Server Started. Listening on (UDP)Port: " +
                                        (this.serverPort));
            //return;
        }
        switch (this.udpOrTcp){
            case "UDP":
                while(! isStopped()){
                    try{
                        byte[] b = new byte[OcpConstants.MAX_UDP_SIZE];
                        DatagramPacket dp = new DatagramPacket(b, b.length);

                        //System.out.println("Receiving next UDP...");
                        this.clientSocketUDP.receive(dp);
                        System.out.println("UDP Received!");
                        
                        
                        System.out.print("Processing message...");
                        
                        new Thread(
                                new WorkerRunnable(
                                    this.clientSocketUDP, dp, "OCP Server(UDP)")
                            ).start();
                    	}
                    	catch(Exception e){
                    		e.printStackTrace();
                    	}
                }
                break;
            case "TCP" :
                while(! isStopped()){
                    Socket clientSocketTCP = null;
                    try {
                        clientSocketTCP = this.serverSocket.accept();
                    } catch (IOException e) {
                        if(isStopped()) {
                            System.out.println("TCP Server Stopped.") ;
                            return;
                        }
                        throw new RuntimeException(
                            "Error accepting client TCP connection", e);
                    }
                    new Thread(
                        new WorkerRunnable(
                            clientSocketTCP, "OCP Server(TCP)")
                    ).start();
                }
                break;
            default:
                System.out.println("Unable to start " + this.udpOrTcp + 
                        " mystery server.");
        }
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop(){
        isStopped = true;
        try {
            this.serverSocket.close();
            this.clientSocketUDP.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        } catch (NullPointerException npe){
        	System.out.println("npe");
            return;
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            //this.serverDatagramSocket = new DatagramSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port" + serverPort, e);
        }
    }
    
    private void openDatagramServerSocket() {
        try {
            this.clientSocketUDP = new DatagramSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port" + serverPort, e);
        }
    }
    
    public static void main(String[] args){
    	OcpServer server = new OcpServer(OcpConstants.OCP_PORT, "TCP");
    	OcpServer serverUDP = new OcpServer(OcpConstants.OCP_PORT_PLUS_1, "UDP");
        new Thread(server).start();
        new Thread(serverUDP).start();
        
    	try {
    		Scanner input = new Scanner(System.in);
    		String q = "";
    		while(true){
    			q = input.nextLine();
    			if (q.equals("q")){
    				break;
    			}
    		}
    	    
    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
    	System.out.println("Stopping Server");
    	server.stop();
    }

}