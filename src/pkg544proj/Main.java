/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg544proj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
//import java.util.Scanner;
 /*
 * Group 6 - Jeromy Guinther, Frederick Montgomery, Rod Aluise, Hao Chang
 * 6/3/2016
 * Main class
 * This is the Main class which contains the UI interface for the Client as
 * well as the hooks into the client itself. It begins by asking the user for a server.
 * The only accepted server right now is a "well known" server known as server1.
 * 
 * After having a server, it will ask for a tag. After it gets this tag, it will 
 * open a client and begin the process of obtaining relevant highlights.
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
            String input = "";
            String[] inputArray = null;
            BufferedReader br = new BufferedReader
                    (new InputStreamReader(System.in));
            int numInputArgs = 0;
// CHECK IF USER ENTERED PARAMETERS OPTION IS CHECKED.
        if (args.length != 0) {
            if (args[0].equals("-u")){
    // GET USER INPUT IF COMMAND LIST SPECIFIED.
                
                while(numInputArgs == 0){
                    System.out.println
                        ("Enter your highlight request in the following format:");
                    System.out.println
                        ("[__<server_name>] [__<ip address>] "
                            + "<tag 1> [<tag 2>] ... [<tag n>]");
                    System.out.println
                        ("(or type '__h' for help)");
                    try{
                        input = br.readLine();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    inputArray = input.split(" ");
                    numInputArgs = inputArray.length;
                    if (inputArray[0].equals("q")) break;
                }
    // OTHERWISE SET DEFAULT INPUT FOR TESTING FOR NOW
    // THIS IS WHAT I'VE BEEN TESTING WITH SO FAR.
            }else{
               for (int i=0; i < args.length; i++){
                   input = input + args[i].toString() + " ";
               }
               //input = "__127.0.0.1 sixers lakers";//localhost
               //input = "__129.25.8.226 sixers lakers" //tux?
               inputArray = input.split(" ");
               numInputArgs = inputArray.length;
           }
        }else{
        	
        	while(!input.equals("server1")){
        		System.out.println("Input the name of the highlights server you wish to query or q and enter to quit:");
        		try {
        			input = br.readLine();
        		} catch (IOException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}
        		if(!input.equals("server1")){
        			System.out.println("Invalid server selected.");
        		}
        		if(input.equals("q")){
        			System.out.println("Bye!");
        			return;
        		}
        	}
        	System.out.println("Input the name of a highlights tag you wish to search for and press enter or type q and enter to quit:");
        	try{
                input = br.readLine();
            }catch (Exception e){
                e.printStackTrace();
            }
        	if(input.equals("q")){
    			System.out.println("Bye!");
    			return;
    		}
            String[] tempStringArray = {"127.0.0.1", input};
            inputArray = tempStringArray;
        }
       
// SET SERVER NAME AND TAGS BASED ON USER INPUT
        String serverName = "";
        String[] tagArray;
        if (inputArray[0].startsWith("__")){
            //Hard code the server right now.
            tagArray = Arrays.copyOfRange(inputArray,1, numInputArgs);
            //serverName = "127.0.0.1";
            serverName = inputArray[0].substring(2);
        }
        else{
            serverName = "127.0.0.1"; //local
            //serverName = "129.25.8.226";//tux??
            tagArray = inputArray;
        }

//CREATE NEW OCP CLIENT FROM USER INPUT
// FOR TESTING THIS ASSUMES SERVER IS ALREADY RUNNING.        
        OcpClient ocpClient = new OcpClient(serverName, tagArray);
        System.out.println("Goodbye.");
        
        //OcpServer.main(args);
    }
    
}
